module gitlab.com/pisauridae/core

require (
	github.com/1-bi/uerrors v0.0.0-20181130092621-750e9e1e6077 // indirect
	github.com/antchfx/htmlquery v0.0.0-20181125090355-f40d0ac44926
	github.com/antchfx/xpath v0.0.0-20181126034058-1239a4a2acfc // indirect
	github.com/cespare/xxhash v1.1.0
	github.com/chromedp/cdproto v0.0.0-20181201113921-96cd0ad398e9 // indirect
	github.com/chromedp/chromedp v0.1.2
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/disintegration/imaging v1.5.0 // indirect
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/gorilla/websocket v1.4.0 // indirect
	github.com/jolestar/go-commons-pool v2.0.0+incompatible
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	golang.org/x/image v0.0.0-20181116024801-cd38e8056d9b // indirect
	golang.org/x/net v0.0.0-20181201002055-351d144fa1fc
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
