package models

import (
	"github.com/cespare/xxhash"
	"gitlab.com/pisauridae/core/models/schema"
	"strconv"
)

/**
 * contruct base define model for repository
 * @deplecared
 */
type UrlReqModel struct {
	schema.UrlReq
}

func (this *UrlReqModel) GetReqId() (uint64, error) {
	reqId, err := strconv.ParseUint(this.ReqId, 16, 64)
	return reqId, err
}

func (this *UrlReqModel) SetReqId(reqid uint64) {
	this.ReqId = strconv.FormatUint(reqid, 16)

}

// =================== contruct model for [  UrlReqModel ] =====================
func NewUrlReqModel(url string, taskcode string) *UrlReqModel {
	req := new(UrlReqModel)
	req.Url = url
	req.Taskcode = taskcode
	req.SetReqId(xxhash.Sum64String(url))
	return req
}

// =================== contruct model for [  Task ] =====================
type TaskModel struct {
	schema.Task
}

func (myself *TaskModel) GetTaskcode() string {
	return myself.Taskcode
}

func (myself *TaskModel) SetTaskcode(taskcode string) {
	myself.Taskcode = taskcode
}

func (myself *TaskModel) GetEntryReqs() []string {
	return myself.EntryReqs
}

func (myself *TaskModel) SetEntryReqs(reqs []string) {
	myself.EntryReqs = reqs
}

// =================== contruct model for [  TaskInstnace ] =====================
type TaskInstnaceModel struct {
	schema.TaskInstnace
}

func (myself *TaskInstnaceModel) GetId() string {
	return myself.Id
}

func (myself *TaskInstnaceModel) SetId(id string) {
	myself.Id = id
}

func (myself *TaskInstnaceModel) GetTaskcode() string {
	return myself.Taskcode
}

func (myself *TaskInstnaceModel) SetTaskcode(taskcode string) {
	myself.Taskcode = taskcode
}

func (myself *TaskInstnaceModel) GetStatus() byte {
	return myself.Status
}

func (myself *TaskInstnaceModel) SetStatus(status byte) {
	myself.Status = status
}

func (myself *TaskInstnaceModel) GetStartTime() int64 {
	return myself.StartTime
}

func (myself *TaskInstnaceModel) SetStartTime(startTime int64) {
	myself.StartTime = startTime
}

func (myself *TaskInstnaceModel) GetEndTime() int64 {
	return myself.EndTime
}

func (myself *TaskInstnaceModel) SetEndTime(endTime int64) {
	myself.EndTime = endTime
}

// SettingPropsModel all properties for setting
type SettingPropsModel struct {
	Props map[string]interface{}
}

func (myself *SettingPropsModel) Set(key string, val interface{}) {
	myself.Props[key] = val
}

func (myself *SettingPropsModel) Get(key string) interface{} {
	return myself.Props[key]
}

func (myself *SettingPropsModel) Remove(key string) {
	delete(myself.Props, key)
}

const (
	INST_READY       int = 0
	INST_RUNNING     int = 1
	INST_END         int = 2
	INST_PAUSE       int = 8
	INST_NOTCOMPLETE int = 9
)

// =================== contruct model for [  TaskEntryUrl ] =====================
type RequestUrlModel struct {
	schema.RequestUrl
}

func (myself *RequestUrlModel) GetId() string {
	return myself.Id
}

func (myself *RequestUrlModel) SetId(id string) {
	myself.Id = id
}

func (myself *RequestUrlModel) GetTaskcode() string {
	return myself.Taskcode
}

func (myself *RequestUrlModel) SetTaskcode(taskcode string) {
	myself.Taskcode = taskcode
}

func (myself *RequestUrlModel) GetStatus() byte {
	return myself.Status
}

func (myself *RequestUrlModel) SetStatus(status byte) {
	myself.Status = status
}

func (myself *RequestUrlModel) GetUrl() string {
	return myself.Url
}

func (myself *RequestUrlModel) SetUrl(url string) {
	myself.Url = url
}

func (myself *RequestUrlModel) SetCreateTimestampUnix(unixTimestamp int64) {
	myself.CreateTimestampUnix = unixTimestamp
}

func (myself *RequestUrlModel) GetCreateTimestampUnix() int64 {
	return myself.CreateTimestampUnix
}

func (myself *RequestUrlModel) SetParentId(parentId string) {
	myself.ParentId = parentId
}

func (myself *RequestUrlModel) GetParentId() string {
	return myself.ParentId
}

func (myself *RequestUrlModel) SetLevel(level int16) {
	myself.Level = level
}

func (myself *RequestUrlModel) GetLevel() int16 {
	return myself.Level
}

func (myself *RequestUrlModel) GetHashId() string {
	return myself.HashId
}

func (myself *RequestUrlModel) SetHashId(hashId string) {
	myself.HashId = hashId
}

func (myself *RequestUrlModel) GetInstId() string {
	return myself.InstId
}

func (myself *RequestUrlModel) SetInstId(instId string) {
	myself.InstId = instId
}

const (
	ENTRYURL_PENDING int = 0
	ENTRYURL_RUNNING int = 1
	ENTRYURL_END     int = 2
	ENTRYURL_PAUSE   int = 8
)
