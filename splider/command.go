package splider

const (
	CMD_ADD_URL_REQUEST = 1
)

/**
 * contruct cmd object
 */
type CmdMsg struct {
	// define cmd id
	Cmd int8
	// contruct ddata
	Data interface{}
}
