package splider

import (
	"bytes"
	"errors"
	"reflect"
	"time"
)

const (
	MODULE_PREFIX = "splider"

	CHANNEL_CMD_QUEUE = "cmd_tasks"

	QUEUE_DEFAULT = "default"

	STATE_TODO    = "todo"
	STATE_RUNNING = "running"
	STATE_SUCCESS = "success"
	STATE_FAILURE = "failure"

	EVENT_REQ_PROD_IN_BRAND = "EVENT_REQ_PROD_IN_BRAND"

	RUNTIME_MODE    = "mode"
	RUNTIME_PROFILE = "profile"

	RUNTIME_BASEDIR = "runtime.basedir"
	RUNTIME_DATADIR = "runtime.dataPath"
	RUNTIME_LOGDIR  = "runtime.logPath"

	//  event lifecycle define
	ELC_START_TASK   = "task#start"
	ELC_EXEC_REQ     = "request.task#exec"
	ELC_COMPLETE_REQ = "request.task#complete"
)

var (

	//SE_TIMEOUT_DEFAULT = 150 * time.Millisecond
	SE_TIMEOUT_DEFAULT = 3 * time.Second
)

func Call(fn interface{}, params ...interface{}) (result []reflect.Value, err error) {
	f := reflect.ValueOf(fn)
	if len(params) != f.Type().NumIn() {
		err = errors.New("The number of params is not adapted.")
		return
	}
	in := make([]reflect.Value, len(params))
	for k, param := range params {
		in[k] = reflect.ValueOf(param)
	}
	result = f.Call(in)
	return
}

var runtimeInst = make(map[string]interface{}, 0)

func GetRuntimeProfile() map[string]interface{} {
	return runtimeInst
}

/**
 * ---- defind value bean ---
 */
type Task struct {
	Code      string   `json:"code"`
	EntryUrls []string `json:"entryUrls"`
	ProcName  string   `json:"procName"`
	Pipelines []string `json:"pipelines""`
}

/**
 * persiste the splider task data to data base
 */
type SpliderTaskModel struct {
	Taskcode      string
	Pipelines     []string
	ProcessorKeys []string
}

/**
 * define the data for http req / res
 */
type DataRes struct {
	Mod            string      `json:"module"`
	Code           string      `json:"code"`
	MessageContent string      `json:"message"`
	Callback       string      `json:"callback,omitempty"`
	Data           interface{} `json:"data,omitempty"`
}

func (this *DataRes) GetMode() string {
	return this.Mod
}

func (this *DataRes) GetCode() string {
	return this.Code
}

func (this *DataRes) GetMessageContent() string {
	return this.MessageContent
}

func (this *DataRes) GetData() interface{} {
	return this.Data
}

func (this *DataRes) GetCallback() string {
	return this.Callback
}

func (this *DataRes) SetMode(mod string) {
	this.Mod = mod
}

func (this *DataRes) SetCode(code string) {
	this.Code = code
}

func (this *DataRes) SetMessageContent(msgContent string) {
	this.MessageContent = msgContent
}

func (this *DataRes) SetData(data interface{}) {
	this.Data = data
}

func (this *DataRes) SetCallback(callback string) {
	this.Callback = callback
}

func NewResData(mod string, code string, allMsg ...string) *DataRes {
	dataRes := new(DataRes)
	dataRes.Mod = mod
	dataRes.Code = code

	buf := bytes.NewBufferString("")

	for _, msg := range allMsg {
		buf.WriteString(msg)
	}

	dataRes.MessageContent = buf.String()
	return dataRes
}
