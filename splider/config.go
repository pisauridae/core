package splider

/**
 * set the db config
 */
type DbConf struct {
	Type          string
	Schema        string
	Url           string
	Authorization Authorization
}

type Authorization struct {
	Mode     string
	User     string
	Password string
}

type BrokerConf struct {
	Endpoints string
	Port      int
	Settings  map[string]string
}

type DistributionConf struct {
	Mqbroker    BrokerConf
	Statebroker BrokerConf
}

type PropConf struct {
	Key   string
	Value string
}

/**
 * set the splider config
 */
type SpliderConf struct {
	Basedir      string           `yaml:"basedir,inline"`
	NodeId       string           `yaml:"node-id,inline"`
	Db           DbConf           `yaml:"db,inline"`
	Distribution DistributionConf `yaml:"distribution,inline"`
}

/**
 * =========================  rule confile ================================
 */
type ScriptMap struct {
	Url        string
	ParamsName []string `yaml:"params-name"`
	ScriptFile string   `yaml:"script"`
}

type RuleConf struct {
	PreLinked    []string     `yaml:"pre-links"`
	MatchScripts []*ScriptMap `yaml:"match-scripts"`
}

type UserAccount struct {
	username string `yaml:"login-name"`
	password string `yaml:"password"`
}

type TaskRuleProfile struct {
	Rulecode     string
	basedir      string
	Download     *RuleConf      `yaml:"donwload"`
	Processor    *RuleConf      `yaml:"process"`
	Pipeline     *RuleConf      `yaml:"pipeline"`
	userAccounts []*UserAccount `yaml:"user-accounts"`
}

func (this *TaskRuleProfile) GetBaseDir() string {
	return this.basedir
}
