package splider

import (
	"github.com/antchfx/htmlquery"
	"gitlab.com/pisauridae/core/models"
	"golang.org/x/net/html"
	"strings"
)

/**
 * construct page context
 */
type Page struct {
	html                string
	candidateReqs       *models.UrlReqModel
	downloadSuccessFlag bool
	resultItems         map[string]interface{}
	linkedContext       *LinkedContext
}

/**
 * init base object
 */
func init() {

}

func BuildPage(sreq *models.UrlReqModel) *Page {
	var page *Page
	page = new(Page)
	page.candidateReqs = sreq
	page.resultItems = make(map[string]interface{}, 0)

	page.linkedContext = &LinkedContext{}
	page.linkedContext.newLinkedUrls = make([]string, 0)

	return page
}

/**
 * contruct html
 */
func (this *Page) GetHtml() string {
	return this.html
}

func (this *Page) SetHtml(html string) {
	this.html = html
}

func (this *Page) IsDownloadSuccess() bool {
	return this.downloadSuccessFlag
}

func (this *Page) SetDownloadSuccess(flag bool) {
	this.downloadSuccessFlag = flag
}

func (this *Page) GetPageUrl() string {
	return this.candidateReqs.Url
}

func (this *Page) GetSpliderRequest() *models.UrlReqModel {
	return this.candidateReqs
}

func (this *Page) GetLinkedContext() *LinkedContext {
	return this.linkedContext
}

/**
 *
 */
func (this *Page) GetResultItem(key string) interface{} {
	return this.resultItems[key]
}

func (this *Page) GetResultItems() map[string]interface{} {
	return this.resultItems
}

func (this *Page) AddResultItem(key string, value interface{}) error {
	// --- check the key existed ----

	// --- set the value ---
	this.resultItems[key] = value
	return nil
}

func (this *Page) GetHtmlDoc() (*html.Node, error) {

	doc, err := htmlquery.Parse(strings.NewReader(this.html))

	if err != nil {
		return nil, err
	}

	return doc, nil

}

// =========================== add update ===============================
