package splider

type LinkedContext struct {
	newLinkedUrls []string
}

func (this *LinkedContext) AddLinkedUrl(newUrl string) {
	this.newLinkedUrls = append(this.newLinkedUrls, newUrl)
}

func (this *LinkedContext) GetAllLinkedUrl() []string {
	return this.newLinkedUrls
}
