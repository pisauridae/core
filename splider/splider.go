package splider

import (
	"gitlab.com/pisauridae/core/models"
)

/**
 * define the base splider task
 */
type SpliderTask struct {
	Taskcode      string
	Pipelines     []string
	ProcessorKeys []string
}

/**
 * define interface
 */
type SpliderContext interface {

	/**
	 * add url request for context

	 */

	GetCurrentTask() *SpliderTask
}

/**
 * contruct pipe line interface
 */
type Pipeline interface {

	/**
	 * execute pre process function
	 */
	PreProcess(resultItems map[string]interface{}, eventCtx EventCtx) error

	Process(resultItems map[string]interface{}) error

	/**
	 * execute post process function
	 */
	PostProcess(resultItems map[string]interface{}, eventCtx EventCtx) error
}

/**
 * call and defined event
 */
type EventCtx interface {
	FireEvent(eventName string, arguments map[string]interface{}) error
}

/**
 * create manager
 */
type ReqUrlSessionFactory interface {

	/**
	*
	 */
	Create() ReqUrlSession
}

/**
 * defined handle message inteface
 */
type ReqUrlSession interface {

	/**
	 * open client
	 */
	Open() error

	SendRequest(sreq *models.UrlReqModel) error

	UpdateTaskStatus(req *models.UrlReqModel, fromState string, toState string) error

	/**
	 * get the instance by request id
	 */
	GetByFullKey(reqIdFullKey string) (*models.RequestUrlModel, error)

	Flush()

	Close()
}
