package splider

import "github.com/1-bi/uerrors"

const (
	_prefix = "splider: "
)

var (
	ErrConnectMongon       = uerrors.NewCodeErrorWithPrefix("splider", "00000001")
	ErrServiceBusMngNotNil = uerrors.NewCodeErrorWithPrefix("splider", "errServiceBusMngNotNil", "The service bus manager is not null ! ")
	ErrNodemodeNotSupport  = uerrors.NewCodeErrorWithPrefix("splider", "errNodeNotSupport", "The argument 'node' is not support! ")
	ErrNotFoundProcRules   = uerrors.NewCodeErrorWithPrefix("splider", "errNotFoundProcRules", "Could not find the js_rules by task code : {taskcode} ")
	ErrDownloadPage        = uerrors.NewCodeErrorWithPrefix("splider", "errDownloadPage", "Some error found in downloading : {content}")
)
