package test

import (
	"context"
	"fmt"
	common_pool "github.com/jolestar/go-commons-pool"
	"gitlab.com/pisauridae/core/test/pool_mock"
	"gitlab.com/pisauridae/core/utils_pool"
	"testing"
)

/**
 * use mock pool 1o
 */
func Test_PoolBuilder_Define_Case1(t *testing.T) {
	pb := utils_pool.NewPoolBuilder()

	// ---- create pool mock object 1 ---
	factory := common_pool.NewPooledObjectFactorySimple(
		func(context.Context) (interface{}, error) {
			return &pool_mock.PoolMockObject1{
				StringContent: "MockPool1",
			}, nil
		})

	ctx := context.Background()
	p := common_pool.NewObjectPoolWithDefaultConfig(ctx, factory)

	pb.DefinedPool(p, "mockObj", nil)

	mockPool, err := pb.GetPoolById("mockObj")

	ctx2 := context.Background()

	obj, err := mockPool.BorrowObject(ctx2)
	if err != nil {
		panic(err)
	}

	o := obj.(*pool_mock.PoolMockObject1)
	fmt.Println("define this object ")
	fmt.Println(o.StringContent)

}
