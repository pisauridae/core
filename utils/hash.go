package utils

import (
	"github.com/cespare/xxhash"
	"strconv"
)

func GetHashId(in string) string {
	var result uint64
	result = xxhash.Sum64String(in)

	return strconv.FormatUint(result, 16)
}
