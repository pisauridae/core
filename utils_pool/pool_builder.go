package utils_pool

import (
	"errors"
	common_pool "github.com/jolestar/go-commons-pool"
)

type PoolBuilder struct {
	poolHolder map[string]*common_pool.ObjectPool
}

func NewPoolBuilder() *PoolBuilder {
	pb := new(PoolBuilder)
	pb.poolHolder = make(map[string]*common_pool.ObjectPool, 0)

	return pb
}

func (this *PoolBuilder) DefinedPool(opool *common_pool.ObjectPool, poolId string, cfgObj *common_pool.ObjectPoolConfig) error {

	// --- check the id exsit ---
	existed := this.CheckPoolById(poolId)
	if existed {
		return errors.New("Pool by id \"" + poolId + "\" has existed. Please remove this pool first . ")
	}

	// --- add by id ---
	this.poolHolder[poolId] = opool

	return nil
}

func (this *PoolBuilder) RemovePoolById(poolId string) {
	delete(this.poolHolder, poolId)
}

/**
 * return true if pool existd by id
 */
func (this *PoolBuilder) CheckPoolById(poolId string) bool {
	obj := this.poolHolder[poolId]
	return obj != nil
}

func (this *PoolBuilder) GetPoolById(poolId string) (*common_pool.ObjectPool, error) {

	existed := this.CheckPoolById(poolId)
	if !existed {
		return nil, errors.New("Pool by id \"" + poolId + "\" not existed . ")
	}

	return this.poolHolder[poolId], nil
}
